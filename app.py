from flask import *

app = Flask('Simple test web app')

@app.route('/')
def hello():
	return 'Hello world from cloud.'

if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0', port=5555)
